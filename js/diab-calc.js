const diabTabsContainer = $('#diab-calc-tabs-container');
const diabTabsContentContainer = $('#diab-calc-tabs-content-container');

const diabTabsPaginationContainer = $('#diab-calc-mob-pagination-container');

const diabResultContainer = $('#diab-calc-result-container');
const diabResultDescriptionContainer = $('#diab-calc-result-description');
const diabResultHeadingContainer = $('#diab-calc-result-heading');
const diabResultLowDescriptionHeadingContainer = $('#diab-calc-result-low-heading');
const diabResultHighDescriptionHeadingContainer = $('#diab-calc-result-high-heading');
const diabResultLowDescriptionBodyContainer = $('#diab-calc-result-low-body');
const diabResultHighDescriptionBodyContainer = $('#diab-calc-result-high-body');
const diabResultSummaryValueContainer = $('#diab-calc-result-summary-value');

const diabPreviousButton = $('.diab-calc-previous-button');
const diabNextButton = $('.diab-calc-next-button');

let diabSelectedTab = 0;

const diabQuestionnaire = [
    {
        type: 'question',
        question: 'ما هو عمرك؟',
        options: [
            '35 عاماً أو أقل',
            '35 - 44 عاماً',
            '45 - 54 عاماً',
            '55 - 64 عاماً',
            '65 عاماً أو أكبر'
        ]
    },
    {
        type: 'question',
        question: 'هل أنت؟',
        options: [
            'أنثى',
            'ذكر'
        ]
    },
    {
        type: 'question',
        question: 'هل لديك أحداً من أفراد العائلة سواء كان أب، أم، أخ، أخت أو طفل مصاب بداء السكري من النوع 1 أو 2؟',
        options: [
            'نعم',
            'لا'
        ]
    },
    {
        type: 'question',
        question: 'هل كنت تعاني من ارتفاع نسبة الجلوكوز في الدم (السكر) على سبيل المثال ، أثناء الفحص الطبي أو أثناء المرض أو أثناء الحمل؟',
        options: [
            'نعم',
            'لا'
        ]
    },
    {
        type: 'question',
        question: 'هل تعاني من ارتفاع ضغط الدم أو وصف لك الطبيب أدوية لعلاج ارتفاع ضغط الدم من قبل؟',
        options: [
            'نعم',
            'لا'
        ]
    },
    {
        type: 'question',
        question: 'هل أنت مدخن للسجائر أو أي منتجات تبغ أخرى بشكل يومي؟',
        options: [
            'نعم',
            'لا'
        ]
    },
    {
        type: 'question',
        question: 'كم مرة تأكل الخضروات أو الفاكهة؟',
        options: [
            'كل يوم',
            'أحياناً'
        ]
    },
    {
        type: 'question',
        question: 'تقريباً، هل تمارس ما لا يقل عن 2.5 ساعة من النشاط البدني في الأسبوع (على سبيل المثال ، 30 دقيقة في اليوم ، في 5 أيام أو أكثر في الأسبوع)؟',
        options: [
            'نعم',
            'لا'
        ]
    },
    {
        type: 'question',
        question: 'ما هو مقاس خصرك؟',
        optionChoices: [
            [
                'أقل من 80 سم',
                'من 80 - 90 سم',
                '90 سم أو أكثر'
            ],
            [
                'أقل من 90 سم',
                'من 90 - 100 سم',
                '100 سم أو أكثر'
            ]
        ],
        options: [
            'أقل من 80 سم',
            'من 80 - 90 سم',
            '90 سم أو أكثر'
        ],
    },
    {
        type: 'result',
        label: 'النتيجة'
    },
];

const diabRisks = {
    low_medium: {
        riskLevel: "نسبة خطر الإصابة",
        description: "مجموع النقاط {score} نقاط مما يعني أنك في خطر منخفض للإصابة بمرض السكري من النوع 2 في الخمس سنوات المقبلة، من المهم أن تستمر في الحفاظ على نمط حياتك.",
        riskStartRange: 0,
        riskEndRange: 8,
        lowRangeValues: "منخفضة",
        lowRangeDescription: "النقاط من 0 - 5 فهذا يعني أن يصاب شخص واحد تقريبًا من بين كل 100 شخص بمرض السكري.",
        highRangeValues: "متوسطة",
        highRangeDescription: "النقاط من 6 - 8 فهذا يعني أن يصاب شخص واحد تقريبًا من بين كل 50 شخص بمرض السكري.",
        risk: "منخفضة"
    },
    medium_high: {
        riskLevel: "نسبة خطر الإصابة",
        description: "مجموع النقاط {score} نقطة مما يزيد من خطر إصابتك بداء السكري، قد يساعد تحسين نمط حياتك على تقليل خطر الإصابة بمرض السكري من النوع 2.",
        riskStartRange: 9,
        riskEndRange: 15,
        lowRangeValues: "متوسطة",
        lowRangeDescription: "النقاط من 9 - 11 فهذا يعني أن يصاب شخص واحد تقريبًا من بين كل 30 شخص بمرض السكري.",
        highRangeValues: "مرتفعة",
        highRangeDescription: "النقاط من 12 - 15 فهذا يعني أن يصاب شخص واحد تقريبًا من بين كل 14 شخص بمرض السكري.",
        risk: "متوسطة"
    },
    high: {
        riskLevel: "نسبة خطر الإصابة",
        description: "مجموع النقاط {score} نقطة مما يعرضك لخطر الإصابة بمرض السكري، حاول كل فترة زمنية أن تراجع الطبيب حول إجراء اختبار الجلوكوز في الدم وانت صائم.",
        riskStartRange: 16,
        riskEndRange: 38,
        lowRangeValues: "مرتفعة",
        lowRangeDescription: "النقاط من 16 - 19 فهذا يعني أن يصاب شخص واحد تقريبًا من بين كل 7 أشخاص بمرض السكري.",
        highRangeValues: "مرتفعة جداً",
        highRangeDescription: "النقاط فوق 20 فهذا يعني أن يصاب شخص واحد تقريبًا من بين كل 3 أشخاص بمرض السكري.",
        risk: "عالية"
    }
};

let diabResponses = [];

const diabCreateTabs = () => {
    diabTabsContainer.empty();

    diabQuestionnaire.forEach((tab, index) => diabTabsContainer.append(`<div class='diab-calc-tab${index === diabSelectedTab ? ' active' : ''}${index > diabResponses.length ? ' disabled' : ''}' onClick='diabUpdateSelection(${index})'>${tab.type === 'result' ? tab.label : index + 1}</div>`));

    diabUpdateSelection(0);
};

const diabUpdateTabs = index => {
    $(`.diab-calc-tab`).removeClass('active');
    let _index = 0;

    while(_index <= diabResponses.length) {
        $(`.diab-calc-tab:nth-child(${_index++})`).removeClass('disabled');
    }

    $(`.diab-calc-tab:nth-child(${index + 1})`).removeClass('disabled');
    
    $(`#diab-calc-tabs-container .diab-calc-tab:nth-child(${index + 1})`).addClass('active');

    if(index < 9) diabTabsPaginationContainer.html(`${index + 1}/9`);
    else diabTabsPaginationContainer.html(``);
};

const diabUpdateSelection = index => {
    diabSelectedTab = index;

    diabPreviousButton.removeClass('disabled');
    diabNextButton.addClass('disabled');

    if(index < diabResponses.length) {
        diabNextButton.removeClass('disabled');
    }

    if(index === 0){
        diabPreviousButton.addClass('disabled');
    }

    if(index === 8) {
        diabQuestionnaire[index].options = diabQuestionnaire[index].optionChoices[diabResponses[1]];
    }

    diabUpdateTabs(index);

    diabUpdateContent(index);
};

const diabUpdateContent = index => {
    const questionsContainer = $('#diab-calc-question-container');
    const diabResultContainer = $('#diab-calc-result-container');

    if(diabQuestionnaire[index].type === 'question') {
        diabResultContainer.hide();
        questionsContainer.show();

        diabUpdateQuestion(index);

        diabUpdateOptions(index);
    } else if(diabQuestionnaire[index].type === 'result') {
        questionsContainer.hide();
        diabResultContainer.show();

        diabCalculateRisk();
    }
};

const diabUpdateQuestion = index => {
    $('#diab-calc-tabs-content-container #diab-calc-question-container .diab-calc-question').html(diabQuestionnaire[index].question);
};

const diabUpdateOptions = index => {
    const optionsContainer = $('#diab-calc-tabs-content-container #diab-calc-question-container .diab-calc-options');

    const options = diabQuestionnaire[index].options;

    optionsContainer.empty();
    
    for(let option in options) {
        optionsContainer.append(`<div class='diab-calc-option col-sm-3${diabQuestionnaire[index].selectedOption == option ? " selected" : ""}' onclick='diabAddResponse(${index}, ${option})'>${options[option]}</div>`);
    }
};

const diabAddResponse = (questionIndex, responseIndex) => {
    $(`#diab-calc-tabs-content-container #diab-calc-question-container .diab-calc-options .diab-calc-option`).removeClass('selected');

    diabResponses[questionIndex] = responseIndex;
    diabQuestionnaire[questionIndex].selectedOption = responseIndex;

    $(`.diab-calc-next-button`).removeClass('disabled');

    $(`#diab-calc-tabs-content-container #diab-calc-question-container .diab-calc-options .diab-calc-option:nth-child(${responseIndex + 1})`).addClass('selected');

    $(`#diab-calc-tabs-container .diab-calc-tab:nth-child(${questionIndex + 1})`).html('&check;');
};

const diabCalculateRisk = () => {
    const ageScores = [0, 2, 4, 6, 8];
    const genderScores = [4, 7];
    const familyScores = [3, 0];
    const glucoseScores = [6, 0];
    const bpScores = [2, 0];
    const smokingScores = [2, 0];
    const vegetablesScores = [0, 1];
    const activitiesScores = [0, 2];
    const waistScores = [0, 4, 7];

    const scores = [
        ageScores,
        genderScores,
        familyScores,
        glucoseScores,
        bpScores,
        smokingScores,
        vegetablesScores,
        activitiesScores,
        waistScores
    ];

    let score = 0;

    scores.forEach((_score, index) => {
        score += _score[diabResponses[index]];
    });

    let risk = 'low';

    if(score >= 20) risk = 'high';
    else if(score >= 16 &&  score < 20) risk = 'high';
    else if(score >= 9 &&  score < 16) risk = 'medium_high';
    else if(score >= 0 &&  score < 9) risk = 'low_medium';

    const description = `${diabRisks[risk].description.replace(/{score}/g, score)}`;
    const lowDescription = `${diabRisks[risk].lowRangeDescription.replace(/{score}/g, score)}`;
    const highDescription = `${diabRisks[risk].highRangeDescription.replace(/{score}/g, score)}`;
    const lowHeading = `${diabRisks[risk].lowRangeValues.replace(/{score}/g, score)}`;
    const highHeading = `${diabRisks[risk].highRangeValues.replace(/{score}/g, score)}`;

    diabResultHeadingContainer.removeClass('low_medium');
    diabResultHeadingContainer.removeClass('medium_high');
    diabResultHeadingContainer.removeClass('high');

    diabResultHeadingContainer.addClass(risk);

    diabResultLowDescriptionHeadingContainer.removeClass('low_medium');
    diabResultLowDescriptionHeadingContainer.removeClass('medium_high');
    diabResultLowDescriptionHeadingContainer.removeClass('high');

    diabResultLowDescriptionHeadingContainer.addClass(risk);

    diabResultHighDescriptionHeadingContainer.removeClass('low_medium');
    diabResultHighDescriptionHeadingContainer.removeClass('medium_high');
    diabResultHighDescriptionHeadingContainer.removeClass('high');

    diabResultHighDescriptionHeadingContainer.addClass(risk);

    diabResultDescriptionContainer.html(description);
    diabResultLowDescriptionHeadingContainer.html(lowHeading);
    diabResultHighDescriptionHeadingContainer.html(highHeading);
    diabResultLowDescriptionBodyContainer.html(lowDescription);
    diabResultHighDescriptionBodyContainer.html(highDescription);
    diabResultSummaryValueContainer.html(lowHeading);
};

const diabCalcInit = () => {
    diabCreateTabs();
};

const diabNext = () => {
    if(diabSelectedTab < diabQuestionnaire.length - 1 && (diabResponses[diabSelectedTab] !== null && diabResponses[diabSelectedTab] !== undefined)) {
        diabUpdateSelection(diabSelectedTab + 1);
    }
};

const diabPrevious = () => {
    if(diabSelectedTab > 0) {
        diabUpdateSelection(diabSelectedTab - 1);
    }
};

const diabReset = () => {
    diabQuestionnaire.forEach(item => {
        delete item.selectedOption;
    });

    diabResponses = [];
    diabSelectedTab = 0;

    diabUpdateSelection(0);
    diabCreateTabs();
};